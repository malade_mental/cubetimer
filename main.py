#!/usr/bin/env python

# date modules
import time
from datetime import datetime
# ui lib
import urwid
from urwid import AttrMap, AttrWrap, ExitMainLoop, FixedWidget, LineBox, ListBox, MainLoop, Overlay, SolidFill, Text, TextCanvas
from urwid.text_layout import StandardTextLayout
from urwid.graphics import BigText

# core modules
from threading import Thread
import random
import os

# Global loop
mainloop = None

# Helper lambda functions ( because lambda rox )
get_cur_time = lambda : int(time.time() * 1000)
format_ms = lambda ms : datetime.fromtimestamp(ms / 1000).strftime('%M:%S.%f')[0:-3]

class StartButton(urwid.wimp.Button):
# The Start/Stop button class that will trigger our timer
    def __init__(self, timer):
        self.timer = timer
        self.thread = None
        super(StartButton, self).__init__('START')
        self.set_signal(self.start)

    # Event functions
    def unset_signal(self, cb):
        urwid.disconnect_signal(self, 'click', cb)

    def set_signal(self, cb):
        urwid.connect_signal(self, 'click', cb)

    # Main functions
    def start(self, item):
        # Label change
        self.set_label('STOP')

        # Create thread for the timer
        self.thread = Thread(group=None, target=self.timer.start, name='timer')
        self.thread.run()

        # Remove start signal
        self.unset_signal(self.start)
        # Add stop signal
        self.set_signal(self.stop)


    def stop(self, item):
        # Stop timer!
        self.timer.stop()

        # Label change
        self.set_label('START')

        # Remove stop signal
        self.unset_signal(self.stop)
        # Add start signal
        self.set_signal(self.start)

class MyDisplay:
# The display class to load a window
    # coloooors < who R pretty >
    palette = [
        ('body','black','light gray', 'standout'),
        ('border','black','dark blue'),
        ('shadow','white','black'),
        ('selectable','black', 'dark cyan'),
        ('focus','white','dark blue','bold'),
        ('focustext','light gray','dark blue'),
        ]

    # useless atm
    fonts = urwid.get_all_fonts()

    def __init__(self, text, height, width, timer):
    # Nia nia nia example function shit
        width = int(width)
        if width <= 0:
            width = ('relative', 80)
        height = int(height)
        if height <= 0:
            height = ('relative', 50)

        # Timerbox is the body of the filler (who cares)
        self.body = urwid.Filler(timer.timerbox, 'middle')

        self.frame = urwid.Frame( self.body, focus_part='footer')

        # Scramble show
        scramble = Scramble()
        self.scramble_box = Text(' '.join(scramble), align='center')
        self.frame.header = urwid.Pile([  self.scramble_box ])

        # Button initialization and positioning
        self.button = StartButton(timer)
        self.button_container = urwid.AttrWrap(self.button,  'selectable', 'focus')
        self.frame.footer = urwid.Pile([ urwid.Divider(),  urwid.Padding(self.button_container, align='center', width=10) ])

        w = self.frame

        # pad area around listbox
        w = urwid.Padding(w, ('fixed left',2), ('fixed right',2))
        w = urwid.Filler(w, ('fixed top',2), ('fixed bottom',2))
        w = urwid.AttrWrap(w, 'body')

        # "shadow" effect
        w = urwid.Columns( [w,('fixed', 2, urwid.AttrWrap(
            urwid.Filler(urwid.Text(('border','  ')), "top")
            ,'shadow'))])
        w = urwid.Frame( w, footer =
            urwid.AttrWrap(urwid.Text(('border','  ')),'shadow'))

        # outermost border area
        w = urwid.Padding(w, 'center', width )
        w = urwid.Filler(w, 'middle', height )
        w = urwid.AttrWrap( w, 'border' )

        self.view = w


class Timer:
# Useful class.
    def __init__(self):
        self.time = format_ms(0)
        self.starttime = None
        self.curtime = None
        self.endtime = None
        self.timerbox = TimerBox(self.time)
        self.started = False
        self.loop = None


    def start(self):
    # Launch!
        self.started = True
        self.starttime = self.curtime = get_cur_time()
        self.updt()

    def updt(self, *args):
    # Run!
        # Don't update if not started!
        if not self.started:
            return

        # Else with update the current time
        self.curtime = get_cur_time() - self.starttime
        # and change the value in the text box
        self.timerbox.set_text(format_ms(self.curtime))

        # This is how we reload the function, with the main "loop" object.
        # the higher the value, the faster the reload
        self.loop.set_alarm_in(0.05, self.updt)
            

    def stop(self):
    # Brk!
        self.started = False
        self.endtime = time.time()
    

class TimerBox(urwid.widget.Text):
# Small child class of Text, for future enhancement
    def __init__(self, txt=''):
        super(TimerBox, self).__init__(txt, align='center')

class Scramble(list):
    sides = ['U','B','D','R','F','L']
    mods = ['\'','','2','w','w\'']
    size = 16

    def __init__(self):
        for m in self.moves():
            self.append(m)

    def moves(self):
        def looseone (d):
            for i in d.keys():
                if d.get(i) > 0:
                    d[i] -= 1

        def iszero (d):
            f = lambda x : d.get(x) == 0
            return [ i for i in filter(f, d.keys()) ]

        dic_sides = dict.fromkeys(self.sides, 0)
        for i in range(0, 16):
            looseone(dic_sides)
            move = random.choice(iszero(dic_sides))
            dic_sides[move] += 3
            yield ''.join((move, random.choice(self.mods))) 

            
            



        

def main():
    try:
        # Initialize the Timer, Display and MainLoop objects
        timer = Timer()
        display = MyDisplay('Test', -1, -1, timer)
        mainloop = MainLoop(display.view
                          , palette = display.palette
                          , unhandled_input = callback)

        timer.loop = mainloop

        # RUNRUNRUN APPAPPAPP #
        mainloop.run() 

    except Exception as e:
        print(e)
        os.sys.exit(0)


def callback(key):
    if key is 'q':
        raise ExitMainLoop()

if __name__ == '__main__':
    main()
